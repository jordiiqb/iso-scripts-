#! /bin/bash
# @edt ASIX M01 CURS 2018-2019
# Exemples case
# -------------------------------------------------

dia=$1

case $dia in
'Dilluns'|'Dimarts'|'Dimecres'|'Dijous'|'Divendres')
  echo "És un dia laboral";;
'Dissabte'|'Diumenge')
  echo "És un dia del cap de setmana";;
*)
  echo "No és un dia";;
esac

exit 0

lletra=$1
case $lletra in
  'a'|'e'|'i'|'o'|'u')
      echo "$lletra és una vocal";;
  *)
      echo "$lletra és una consonant";;
esac
exit 0
