#! /bin/bash
# @edt ASIX M01 CURS 2018-2019
# Jordi Quirós Berbel
# Script que donta una serie de notes per arg diu si aquestes son aprovats o suspensos
# --------------------------------------------------------------------------------------

#1) Validar qu hi hagi com a mínim un arg

if [ $# -lt 1]
then
  echo "ERROR: Number of args didn't reach the minimum"
  echo "USAGE prog arg[...]"
  exit ERR_ARG

#2) Validem les notes introduides

for arg in $*
do
  if ! [ $arg -ge 1 -a $arg -le 10 ]
  then
    echo "Not a correct mark."
  
  else if [ $arg -lt 5 ]
  then
    echo "$arg és un suspés"
   
  else if
  then
    echo "$arg és un aprovat"

  fi
done
