#! /bin/bash
# @edt ASIX M01 CURS 2018-2019
# Exemples case
# -------------------------------------------------

ERR_ARG=1
ERR_ARGVAL=2
OK=0

#validar si existeix argument

if [ $# -ne 1 ]
then
  echo "Error! More than 1 arg"
  echo "USAGE: prog arg1"
  exit $ERR_ARG
fi
#validar si help

if [ "$1" = "-h" -o "$1" = "--help" ]
then
  echo "@edt ASIX M01"
  echo "USAGE: prog.sh mes"
  exit $OK
fi

#validar arg pren valors entre 1 i 12

mes=$1

if [ $mes -lt 1 -o $mes -gt 12 ]
then
  echo "Error! Not a proper month"
  echo "USAGE month between 1 and 12"
  exit $ERR_ARGVAL
fi

#XIXA
case $mes in
'4'|'6'|'9'|'11')
  echo "Té 30 dies";;
'2')
  echo "Té 28 dies";;
*)
  echo "Té 31 dies";;
esac

exit $OK

