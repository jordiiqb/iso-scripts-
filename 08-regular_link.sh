#! /bin/bash
# @edt ASIX M01 CURS 2018-2019
# Validar si l'argument rebut es un directori o no i dir-ho
# ----------------------------------------------------------

#Validem si té un arg
ERR_NARGS=1

if [ $# -ne 1 ]
then
  echo "ERROR: Number of args does not match correct usage."
  echo "USAGE: prog nota"
  exit $ERR_NARGS
fi

if [ "$1" = "-h" -o "$1" = "--help" ]
then
  echo "@edt ASIX M01"
  echo "USAGE: prog dir/regular_file/link"
  exit 0
fi

#Xixa

arg=$1
if [ -d $arg ]
then
  echo "$arg és un direcotri"

else if [ -f $arg]
then
  echo "$arg és un regular file"

else if [ -L $arg ]
then
  echo "$arg és un link"

else if [ -e $arg ]
then
  echo "Arg doesn't match any of the types"

else
  echo "Arg doesn't exist"

fi

exit 0
