#! /bin/bash
# @edT ASIX-M01 Curs 2018-2019
# Entrem una nota numérica del 0 al 10 i retornem si està aprovat o suspès
# -------------------------------------------------------------------------

#cas on no s'introdueix 1 arg

if [ $# -ne 1 ]
then
  echo "ERROR: Number of args does not match correct usage."
  echo "USAGE: prog nota"
  exit 1
fi

nota=$1

#cas on no s'introdueix 1 num del 0 al 10

if [ $nota -lt 0 -o $nota -gt 10 ]
then
  echo "ERROR: Number out of range."
  echo "USAGE: Input number between 0 and 10."
  exit 2
fi

#Xixa

if [ $nota -lt 5 ]
then
  echo "Suspès."
else
  echo "Aprovat."
fi

exit 0
