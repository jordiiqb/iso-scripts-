#! /bin/bash
# @edt ASIX M01 CURS 2018-2019
# Validar si l'argument rebut es un directori o no i dir-ho
# ----------------------------------------------------------

#Validem si té un arg
ERR_NARGS=1

if [ $# -ne 1 ]
then
  echo "ERROR: Number of args does not match correct usage."
  echo "USAGE: prog nota"
  exit $ERR_NARGS
fi

#Xixa

dir=$1

if [ -d $dir ]
then
  echo "$dir és un direcotri"

else
  echo "$dir no és un direcotri"

fi

exit 0
