#! /bin/bash
# @edt ASIX-M01 Curs 2018-2019
# Validar exactament que té 2 args i mostrar-los
# -----------------------------------------------

#si num args no es correcte
if [ $# -ne 2 ]
then
   echo "ERROR: num args incorrecte"
  	echo "USAGE: prog arg1 arg2"
   exit 1
fi	

#xixa

echo "Els dos args son:$1, $2 "
exit 0
