#! /bin/bash
# @edt ASIX M01 CURS 2018-2019
# Jordi Quirós Berbel
# Exemples de bucles for
# ------------------------------------

#mostrar i numerar una llista d'arguments

llista=$@
num=1

for arg in $llista
do
  echo "$num $arg"
  num=$((num+1))
done

exit 0



#iterar per la llista d'arguments

for arg in "$@"
do
	  echo $arg
  done

  exit 0

#iterar per la llista d'arguments

for arg in "$@"
do
  echo $arg
done

exit 0

#iterar per els elements resultat de fer una ordre (ls)

for nom in $(ls) 
do
  echo $nom
done
  
exit 0

#iterar per els elements guardats en una variable

llista="pere pau marta anna"

for nom in $llista
do
  echo $nom
done
exit 0

#iterar per diferents elements

for nom in "pere" "pau" "marta" "anna"
do
  echo $nom
done

